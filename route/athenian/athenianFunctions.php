<?php

function encrypt($text, $key1, $key2, $alph)
{
	require_once($_SERVER['DOCUMENT_ROOT'] . "/includes/alphabets.php");

	$alpCur = ($alph == "ru") ? $ru : $eng;

	$text = preg_split('//u', mb_strtolower(str_replace(' ', '', $text)), -1, PREG_SPLIT_NO_EMPTY);

	$keyText = [];
	foreach ($text as $key => $value) {
		for ($i = 0; $i < count($alpCur); $i++) {
			if ($value == $alpCur[$i]) {
				$keyText[] = $i;
			};
		}
	}

	$textKey = [];
	foreach ($keyText as $key => $value) {
		$textKey[] = ($value*$key1 + $key2) % count($alpCur);
	}

	$textFinal = [];
	foreach ($textKey as $value) {
		foreach ($alpCur as $key => $item) {
			if ($value == $key) {
				$textFinal[] = $item;
			}
		}
	}

	include("resultShow.php");
}

function decrypt($text, $key1, $key2, $alph)
{
	require_once($_SERVER['DOCUMENT_ROOT'] . "/includes/alphabets.php");

	$alpCur = ($alph == "ru") ? $ru : $eng;

	$text = preg_split('//u', mb_strtolower(str_replace(' ', '', $text)), -1, PREG_SPLIT_NO_EMPTY);

	$keyText = [];
	foreach ($text as $key => $value) {
		for ($i = 0; $i < count($alpCur); $i++) {
			if ($value == $alpCur[$i]) {
				$keyText[] = $i;
			};
		}
	}


	for ($i = 0; $i < count($alpCur); $i++){
		if (($key1 * $i % count($alpCur)) == 1){
			$key1 = $i;
			break;			
		}
	}
	

	$textKey = [];
	foreach ($keyText as $key => $value) {
		if (($value - $key2) * (count($alpCur) - $key1) % count($alpCur) < 0) {
			$textKey[] = ($value - $key2) * ($key1) % count($alpCur) + count($alpCur);
		} else {
			$textKey[] = ($value - $key2) * ($key1) % count($alpCur);
		}
		
	}
	
	$textFinal = [];
	foreach ($textKey as $value) {
		foreach ($alpCur as $key => $item) {
			if ($value == $key) {
				$textFinal[] = $item;
			}
		}
	}

	include("resultShow.php");
}

function gcd ($a, $b) {
	if ($a === 0) {
		return abs($b);
	}
	if ($b === 0) {
		return abs($a);
	}
	for ( ; ; ) {
		if (($a %= $b) === 0)
			return abs($b);
		else if (($b %= $a) === 0)
			return abs($a);
	}
}

/*function bezout_recursive($a, $b){
    '''A recursive implementation of extended Euclidean algorithm.
    Returns integer x, y and gcd(a, b) for Bezout equation:
        ax + by = gcd(a, b).
    '''

    $y = bezout_recursive($b, $a % $b);
    $x = bezout_recursive($b, $a % $b); 
    $g = bezout_recursive($b, $a % $b);
    return ($x, $y - ($a / $b) * $x, $g)
}*/